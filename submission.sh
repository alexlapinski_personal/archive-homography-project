#!/bin/bash

echo "Rectifying Sample Images"
echo " - Bookcase Sample"
python rectify.py images/bookcase.jpg points/bookcase_points.txt 300 100
mv rectified.png generated/bookcase_rectified.png
echo ""
echo " - Bookcase 2 Sample"
python rectify.py images/bookcase_2.jpg points/bookcase_2_points.txt 200 220
mv rectified.png generated/bookcase_2_rectified.png
echo ""
echo " - Laptop Sample"
python rectify.py images/laptop.png points/laptop_screen.txt 300 400
mv rectified.png generated/laptop_rectified.png
echo ""
echo " - Floor Sample"
python rectify.py images/floor.jpg points/floor_points.txt 200 50
mv rectified.png generated/floor_rectified.png
echo ""
echo " - Window Sample"
python rectify.py images/window.jpg points/window_points.txt 200 200
mv rectified.png generated/window_rectified.png

echo ""
echo ""
echo "Running Composite Samples"
echo " - Put panda in laptop"
python composite.py images/panda.png images/laptop.png points/panda_points.txt points/laptop_screen.txt images/panda_mask.png
mv composited.png generated/laptop_panda.png
echo ""
echo " - Put panda on floor"
python composite.py images/panda.png images/floor.jpg points/panda_points.txt points/floor_points.txt images/panda_mask.png
mv composited.png generated/floor_panda.png
echo ""
echo " - Put panda in window"
python composite.py images/panda.png images/window.jpg points/panda_points.txt points/window_points.txt images/panda_mask.png
mv composited.png generated/window_panda.png
echo ""
echo " - Put photo in laptop"
python composite.py images/chichen_itza.jpg images/laptop.png points/chichen_itza_points.txt points/laptop_screen.txt images/chichen_itza_mask.png
mv composited.png generated/laptop_pillars.png
echo ""
echo " - Put photo on floor"
python composite.py images/chichen_itza.jpg images/floor.jpg points/chichen_itza_points.txt points/floor_points.txt images/chichen_itza_mask.png
mv composited.png generated/floor_pillars.png
echo ""
echo " - Put photo in window"
python composite.py images/chichen_itza.jpg images/window.jpg points/chichen_itza_points.txt points/window_points.txt images/chichen_itza_mask.png
mv composited.png generated/window_pillars.png

