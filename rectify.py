import sys

import numpy as np
from scipy.misc import *

from p1_skeleton import rectify

try:
    from util import pick_corrs
    HAVE_CORR_PICKER = True
except:
    HAVE_CORR_PICKER = False


def print_usage():
    print("""USAGE: python rectify.py IMAGE TARGET_HEIGHT TARGET_WIDTH

(Requires Python OpenCV bindings, lets you pick a region with your mouse.)

OR

    python rectify.py IMAGE POINTS_FILE TARGET_HEIGHT TARGET_WIDTH

(You provide a text file with points, one per line, in the form
    X1 Y1
    X2 Y2
    X3 Y3
    X4 Y4
)""")

if __name__ == "__main__":
    if len(sys.argv) not in [4, 5]:
        print_usage()
        exit(1)

    image = imread(sys.argv[1]).astype(np.float32) / 255.
    target_shape = (int(sys.argv[-2]), int(sys.argv[-1]))

    # Use the OpenCV corodinate picker if available,
    # otherwise load from a file.
    if HAVE_CORR_PICKER and len(sys.argv) == 4:
        corrs = pick_corrs([image[:, :, ::-1]])
        if corrs is None:
            print("You must pick all 4 correspondences in each image.")
            exit(1)

        pts1 = np.array(corrs[0])

        print("Region Points:")
        for pt in pts1:
            print(pt[0], pt[1])
    elif not HAVE_CORR_PICKER and len(sys.argv) == 4:
        print("Correspondence picker window could not be created."
              " Are python OpenCV bindings installed/available?")
        exit(1)
    else:
        if len(sys.argv) != 5:
            print_usage()
            exit(1)

        pts1 = np.loadtxt(sys.argv[2])

    rectified = rectify(image, pts1, target_shape)
    imsave("rectified.png", rectified)
