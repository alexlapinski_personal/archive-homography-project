import numpy as np
from math import floor

debug = True

def build_A(pts1, pts2):
    """Constructs the intermediate matrix A used in the computation of an
    homography mapping pts1 to pts2."""
    # Build and return A
    pass

    if pts1.shape[0] != pts2.shape[0]:
        print "Unequal number of points passed into build_A"
        return

    num_pts = pts1.shape[0]

    # The A Matrix is of size 2n by 9
    A = np.zeros([2*num_pts, 9], np.float)

    row = 0
    for pt in xrange(0, num_pts):
        (x1, y1) = pts1[pt]
        (x1prime, y1prime) = pts2[pt]

        A[row][0:3] = [x1, y1, 1]
        A[row][-3:] = [-1*x1prime*x1, -1*x1prime*y1, -1*x1prime]
        A[row+1][3:6] = [x1, y1, 1]
        A[row+1][-3:] = [-1*y1prime*x1, -1*y1prime*y1, -1*y1prime]
        row += 2

    return A


def compute_H(pts1, pts2):
    """Computes an homography mapping one set of co-planar points (pts1)
    to another (pts2)."""
    # Construct the intermediate A matrix.
    A = build_A(pts1, pts2)

    # Compute the symmetric matrix AtA.
    AtA = np.dot(A.T, A)

    # Compute the eigenvalues and eigenvectors of AtA.
    eig_vals, eig_vecs = np.linalg.eig(AtA)

    min_eig_val_index = np.argmin(eig_vals)

    # Return the eigenvector corresponding to the smallest eigenvalue, reshaped
    # as a 3x3 matrix.
    min_eig_vec = eig_vecs.T[min_eig_val_index].reshape(3, 3)
    return min_eig_vec


def bilinear_interp(image, point):
    """
        Looks up the pixel values in an image at a given point using bilinear
        interpolation. point is in the format (x, y)
    """
    x, y = point

    # Compute the four integer corner coordinates (top-left/right,
    # bottom-left/right) for interpolationself.
    tl_x = floor(x)
    tl_y = floor(y)
    tr_x = tl_x + 1
    tr_y = tl_y
    bl_x = tl_x
    bl_y = tl_y + 1
    br_x = tr_x
    br_y = bl_y

    # Cap the values
    if tr_x >= image.shape[1]:
        tr_x = image.shape[1] - 1
        br_x = tr_x

    if bl_y >= image.shape[0]:
        bl_y = image.shape[0] - 1
        br_y = bl_y

    # Compute the fractional part of the point coordinates.
    a = x - tl_x
    b = y - tl_y

    # Interpolate between the top two pixels.
    # (1-a)*f(tl_x, tl_y) + a*f(tr_x, tr_y)
    top = (image[tl_y, tl_x] * (1 - a)) + (image[tr_y, tr_x] * a)

    # Interpolate between the bottom two pixels.
    bottom = (image[bl_y, bl_x] * (1 - a)) + (image[br_y, br_x] * a)

    # Return the result of the final interpolation between top and bottom.
    return top * (1-b) + bottom * b


def warp_homography(source, target_shape, Hinv):
    """Warp the source image into the target coordinate frame using a provided
    inverse homography transformation."""
    # NOTE: This can be done much more efficiently (>10x faster) in Python
    # using a series of numpy array operations as opposed to a for loop.

    # Hints for fast version:
    # * Read about numpy broadcasting rules and reshaping operations.
    # * Look up numpy.mgrid / meshgrid for tips on how to quickly generate an
    #   array containing coordinates of all of the points in an image.
    # * You can modify your bilinear_interp() function to take an array of
    #   points instead of single points. Express the actions in this function
    #   using the aforementioned array operations.

    # Warp the source image to the corresponding coordinate system in the
    # output image by:
    #     * transforming points in the output image to the source
    #       image space (using the homography)
    #     * looking up pixel values in the source image at the
    #       transformed points (using bilinear interpolation)

    width = 0
    height = 0

    for pt in target_shape:
        if pt[0] > width:
            width = int(pt[0])
        if pt[1] > height:
            height = int(pt[1])

    result = np.zeros((height, width, 3))

    source_shape = source.shape
    for x in xrange(width):
        for y in xrange(height):
            source_x, source_y = apply_homography(Hinv, [x, y])
            if source_x < 0 or source_x >= source_shape[1]:
                continue
            if source_y < 0 or source_y >= source_shape[0]:
                continue
            result[y, x] = bilinear_interp(source, [source_x, source_y])

    # return the output image
    return result


def rectify(image, planar_points, target_shape):

    # Use the provided target shape to define four points of a target
    # rectangular region with that shape, the top left being the origin (0,0).
    target_points = get_bounding_box(target_shape)

    # Compute the rectifying homography that warps the planar points to the
    # target rectangular region.
    print "Computing Homography"
    H = compute_H(planar_points, target_points)

    # Test our homography
    pts = []
    print "Applying homography to region"
    for pt in planar_points:
        target_pt = apply_homography(H, pt)
        pts.append(target_pt)

    temp = np.ndarray((4, 2), np.float)
    temp[:] = pts
    pts = temp

    # Apply the rectifying homography to the bounding box of the planar image
    # to find its corresponding bounding box in the rectified space.
    print "Applying homography to bounding box"
    original_bounding_box = get_bounding_box(image.shape)
    transformed_box = [apply_homography(H, pt) for pt in original_bounding_box]
    temp = np.ndarray((4, 2))
    temp[:] = transformed_box
    transformed_box = temp

    # Offset the rectified bounding box such that its minimum point (the top
    # left corner) lies at the origin of the rectified space.
    print "Offsetting bounding box"
    transformed_box = transformed_box - transformed_box[0]

    # Compute the inverse homography to warp between the offset, rectified
    # bounding box and the bounding box of the input image.
    print "Computing inverse homography to select points"
    invH = compute_H(transformed_box, original_bounding_box)

    # Perform inverse warping and return the result.
    print "Warping image to rectify region"
    return warp_homography(image, transformed_box, invH)


def get_bounding_box(shape):
    """
        Return a 4 x 2 matrix of the four corners of a square shape.
        The order of points is top-left, top-right, bottom-right, bottom-left
    """
    width = shape[1]
    height = shape[0]
    bounding_box = np.zeros((4, 2))
    bounding_box[1][0] = width
    bounding_box[2] = [width, height]
    bounding_box[3][1] = height
    return bounding_box


def apply_homography(H, point):
    homogenous_point = np.concatenate((point, np.ones(1)), axis=1)
    homogenous_point = homogenous_point
    transformed_point = np.dot(H, homogenous_point)
    transformed_point = transformed_point / transformed_point[-1]
    return transformed_point[:2]


def blend_with_mask(source, target, mask):
    """Blends the source image with the target image according to the mask.
    Pixels with value "1" are source pixels, "0" are target pixels, and
    intermediate values are interpolated linearly between the two."""

    # Determine the dest size, assume source pasted into target
    dest_width = mask.shape[1]
    dest_height = mask.shape[0]

    dest = np.ndarray((dest_height, dest_width, 3))

    for r in xrange(dest_height):
        for c in xrange(dest_width):
            pixel = mask[r, c]
            if np.all(pixel == 1):
                dest[r, c] = source[r, c]
            elif np.all(pixel == 0):
                dest[r, c] = target[r, c]
            else:
                dest[r, c] = source[r, c] * pixel + target[r, c] * (1 - pixel)

    return dest


def composite(source, target, source_pts, target_pts, mask):
    """Composites a masked planar region of the source image onto a
    corresponding planar region of the target image via homography warping."""
    # Compute the homography to warp points from the target
    # to the source coordinate frame.
    print "Computing Homography to warp image into target scene"
    H = compute_H(source_pts, target_pts)

    # Warp the correct images using the homography.
    print "Warping source to scene plane"
    source_warped = warp_homography(source, target_pts, np.linalg.inv(H))
    print "Warping mask to scene plane"
    mask_warped = warp_homography(mask, target_pts, np.linalg.inv(H))

    print "Extending the width of the mask"
    extended_width = target.shape[1] - mask_warped.shape[1]
    filler = np.zeros((mask_warped.shape[0], extended_width, 3))
    mask_warped = np.concatenate((mask_warped, filler), axis=1)

    print "Extending the height of the mask"
    extended_height = target.shape[0] - mask_warped.shape[0]
    filler = np.zeros((extended_height, mask_warped.shape[1], 3))
    mask_warped = np.concatenate((mask_warped, filler), axis=0)

    # Blend the warped images and return them.
    print "Blending source into target with mask"
    return blend_with_mask(source_warped, target, mask_warped)
